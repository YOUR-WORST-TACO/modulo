<?php
namespace OCA\Modulo\Db;

use JsonSerializable;

use OCP\AppFramework\Db\Entity;

class Form extends Entity implements JsonSerializable {

    protected $name;
    protected $creationdate;
    protected $expiredate;
    protected $description;

    public function jsonSerializable() {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'creationdate' => $this->creationdate,
            'expiredate' => $this->expiredate,
            'description' => $this->description
        ];
    }
}