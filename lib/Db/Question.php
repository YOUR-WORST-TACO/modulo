<?php
namespace OCA\Modulo\Db;

use JsonSerializable;

use OCP\AppFramework\Db\Entity;

class Question extends Entity implements JsonSerializable {

    protected $pagefk;
    protected $questiontext;

    public function jsonSerializable() {
        return [
            'id' => $this->id,
            'pagefk' => $this->pagefk,
            'questiontext' -> $this->questiontext
        ];
    }
}