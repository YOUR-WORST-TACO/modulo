<?php
namespace OCA\Modulo\Db;

use OCP\IDbConnection;
use OCP\AppFramework\Db\QBMapper;

class FormMapper extends QBMapper {
    public function __construct(IDbConnection $db) {
        parent::__construct($db, 'modulo_form', Form::class);
    }
}