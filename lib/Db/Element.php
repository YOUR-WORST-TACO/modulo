<?php
namespace OCA\Modulo\Db;

use JsonSerializable;

use OCP\AppFramework\Db\Entity;

class Element extends Entity implements JsonSerializable {

    protected $questionfk;
    protected $elementnum;

    public function jsonSerializable() {
        return [
            'id' => $this->id,
            'questionfk' => $this->questionfk,
            'elementnum' -> $this->elementnum
        ];
    }
}