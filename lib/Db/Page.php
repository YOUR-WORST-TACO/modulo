<?php
namespace OCA\Modulo\Db;

use JsonSerializable;

use OCP\AppFramework\Db\Entity;

class Page extends Entity implements JsonSerializable {

    protected $formfk;
    protected $pagenum;

    public function jsonSerializable() {
        return [
            'id' => $this->id,
            'formfk' => $this->formfk,
            'pagenum' -> $this->pagenum
        ];
    }
}