<?php
namespace OCA\Modulo\Db;

use JsonSerializable;

use OCP\AppFramework\Db\Entity;

class Element extends Entity implements JsonSerializable {

    protected $realid;

    public function jsonSerializable() {
        return [
            'id' => $this->id,
            'realid' => $this->realid
        ];
    }
}