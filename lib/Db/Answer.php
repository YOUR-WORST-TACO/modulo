<?php
namespace OCA\Modulo\Db;

use JsonSerializable;

use OCP\AppFramework\Db\Entity;

class Answer extends Entity implements JsonSerializable {

    protected $questionfk;
    protected $elementfk;
    protected $userfk;
    protected $answerval;
    protected $answerdate;

    public function jsonSerializable() {
        return [
            'id' => $this->id,
            'questionfk' => $this->questionfk,
            'elementfk' => $this->elementfk,
            'userfk' => $this->userfk,
            'answerval' => $this->answerval,
            'answerdate' => $this->answerdate
        ];
    }
}