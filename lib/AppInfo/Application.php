<?php
/**
 * Created by PhpStorm.
 * User: stafoya
 * Date: 3/19/2019
 * Time: 10:31 AM
 */

namespace OCA\Modulo\AppInfo;

use OCP\AppFramework\App;

class Application extends App {

    /**
     * Application constructor.
     * @param array $urlParams
     */
    public function __construct(array $urlParams = [])
    {
        parent::__construct('modulo', $urlParams);
    }
}