<?php
/**
 * Created by PhpStorm.
 * User: stafoya
 * Date: 3/19/2019
 * Time: 11:24 AM
 */

namespace OCA\Modulo\Migration;

use OCP\DB\ISchemaWrapper;
use OCP\Migration\IOutput;
use OCP\Migration\SimpleMigrationStep;

class Version000000Date201903231300 extends SimpleMigrationStep {
    public function changeSchema(IOutput $output, \Closure $schemaClosure, array $options)
    {
        $schema = $schemaClosure();

        /**
         * Form DB Table
         */
        if (!$schema->hasTable('modulo_form')) {
            $table = $schema->createTable('modulo_form');
            $table->addColumn('id', 'integer', [
                'autoincrement' => true,
                'notnull' => true,
            ]);
            $table->addColumn('name', 'string', [
                'notnull' => true,
                'length' => 64,
            ]);
            $table->addColumn('creationdate', 'string', [
                'notnull' => true,
                'length' => 16,
            ]);
            $table->addColumn('expiredate', 'string', [
                'length' => 16,
            ]);
            $table->addColumn('description', 'string', [
                'length' => 200,
            ]);

            $table->setPrimaryKey(['id']);
        }

        /**
         * Page DB Table
         */
        if (!$schema->hasTable('modulo_page')) {
            $table = $schema->createTable('modulo_page');
            $table->addColumn('id', 'integer', [
                'autoincrement' => true,
                'notnull' => true,
            ]);
            $table->addColumn('formfk', 'integer', [
                'notnull' => true,
            ]);
            $table->addColumn('pagenum', 'integer', [
                'notnull' => true,
            ]);

            $table->setPrimaryKey(['id']);
        }

        /**
         * Question DB Table
         */
        if (!$schema->hasTable('modulo_question')) {
            $table = $schema->createTable('modulo_question');
            $table->addColumn('id', 'integer', [
                'autoincrement' => true,
                'notnull' => true,
            ]);
            $table->addColumn('pagefk', 'integer', [
                'notnull' => true,
            ]);
            $table->addColumn('questiontext', 'string', [
                'notnull' => true,
                'length' => 100,
            ]);

            $table->setPrimaryKey(['id']);
        }

        /**
         * Element DB Table
         */
        if (!$schema->hasTable('modulo_element')) {
            $table = $schema->createTable('modulo_element');
            $table->addColumn('id', 'integer', [
                'autoincrement' => true,
                'notnull' => true,
            ]);
            $table->addColumn('questionfk', 'integer', [
                'notnull' => true,
            ]);
            $table->addColumn('elementnum', 'integer', [
                'notnull' => true,
            ]);

            $table->setPrimaryKey(['id']);
        }

        /**
         * Answer DB Table
         */
        if (!$schema->hasTable('modulo_answer')) {
            $table = $schema->createTable('modulo_answer');
            $table->addColumn('id', 'integer', [
                'autoincrement' => true,
                'notnull' => true,
            ]);
            $table->addColumn('questionfk', 'integer', [
                'notnull' => true,
            ]);
            $table->addColumn('elementfk', 'integer', [
                'notnull' => true,
            ]);
            $table->addColumn('userfk', 'integer', [
                'notnull' => true,
            ]);
            $table->addColumn('answerval', 'string', [
                'notnull' => true,
                'length' => 500,
            ]);
            $table->addColumn('answerdate', 'string', [
                'notnull' => true,
                'length' => 16,
            ]);

            $table->setPrimaryKey(['id']);
        }

        /**
         * User DB Table
         */
        if (!$schema->hasTable('modulo_user')) {
            $table = $schema->createTable('modulo_user');
            $table->addColumn('id', 'integer', [
                'autoincrement' => true,
                'notnull' => true,
            ]);
            $table->addColumn('realid', 'integer');

            $table->setPrimaryKey(['id']);
        }

        $formTable = $schema->getTable('modulo_form');
        $pageTable = $schema->getTable('modulo_page');
        $questionTable = $schema->getTable('modulo_question');
        $elementTable = $schema->getTable('modulo_element');
        $answerTable = $schema->getTable('modulo_answer');
        $userTable = $schema->getTable('modulo_user');

        /**
         * build the fekking foreign key shit here
         */

        if (!$pageTable->hasForeignKey('formfk_c')) {
            $pageTable->addForeignKeyConstraint($formTable, ['formfk'], ['id'], ['onDelete' => 'CASCADE', 'onUpdate' => 'CASCADE'], 'formfk_c');
        }

        if (!$questionTable->hasForeignKey('pagefk_c')) {
            $questionTable->addForeignKeyConstraint($pageTable, array('pagefk'), array('id'), ['onDelete' => 'CASCADE', 'onUpdate' => 'CASCADE'], 'pagefk_c');
        }

        if (!$elementTable->hasForeignKey('questionfk_c')) {
            $elementTable->addForeignKeyConstraint($questionTable, array('questionfk'), array('id'), ['onDelete' => 'CASCADE', 'onUpdate' => 'CASCADE'], 'questionfk_c');
        }

        if (!$answerTable->hasForeignKey('questionfk_c')) {
            $answerTable->addForeignKeyConstraint($questionTable, array('questionfk'), array('id'), ['onDelete' => 'CASCADE', 'onUpdate' => 'CASCADE'], 'questionfk_c');
        }

        if (!$answerTable->hasForeignKey('elementfk_c')) {
            $answerTable->addForeignKeyConstraint($elementTable, array('elementfk'), array('id'), ['onDelete' => 'CASCADE', 'onUpdate' => 'CASCADE'], 'elementfk_c');
        }
        
        if (!$answerTable->hasForeignKey('userfk_c')) {
            $answerTable->addForeignKeyConstraint($userTable, array('userfk'), array('id'), ['onDelete' => 'CASCADE', 'onUpdate' => 'CASCADE'], 'userfk_c');
        }

        return $schema;
    }
}