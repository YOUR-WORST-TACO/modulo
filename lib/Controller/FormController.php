<?php
namespace OCA\Modulo\Controller;

use OCP\IRequest;
use OCP\AppFramework\Http\TemplateResponse;
use OCP\AppFramework\Http\DataResponse;
use OCP\AppFramework\Controller;

class FormController extends Controller {
    private $mapper;
    private $userId;

    public function __construct(string $AppName, IRequest $request, NoteMapper $mapper, $UserId) {
        parent::__construct($AppName, $request);
        $this->mapper = $mapper;
        $this->userId = $UserId;
    }
}